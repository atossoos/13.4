﻿#include <iostream>
#include <string>
#include <algorithm>
#include <map>

using namespace std;


class Players
{
    string name;
    int point;

public:
    void names_of_players()
    {
        int cnt;
        cout << "Введи количество игроков: ";
        cin >> cnt;
        cout << "Введите имя игрока и количество набранных им очков: " << '\n';
        string* names = new string[cnt];
        int* points = new int[cnt];
        for (unsigned i{}; i < cnt; i++)
        {
            cout << "Имя: "; cin >> name;
            cout << "Количество очков: "; cin >> point;
            names[i] = name;
            points[i] = point;

        }
        
        for (int i = 1; i < cnt; ++i)
        {
            for (int r = 0; r < cnt - i; r++)
            {
                if (points[r] < points[r + 1])
                {
                    // Обмен местами
                    int temp = points[r];
                    points[r] = points[r + 1];
                    points[r + 1] = temp;


                    string tempn = names[r];
                    names[r] = names[r + 1];
                    names[r + 1] = tempn;

                }
            }
        }

        for (int i = 0; i < cnt; i++)
        {
            cout << names[i] << " = " << points[i] << '\n';
        }
        delete[] names;
        delete[] points;

    }
    
    
};


int main()
{
    setlocale(LC_ALL, "Russian");
    Players p;
    p.names_of_players();
}

