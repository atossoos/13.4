﻿#include <iostream>

using namespace std;
class Animal
{
public:
    virtual void Voice()
    {
        cout << "Animal Voice: ";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Гав!" << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Мяу!" << "\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "Муууу!" << "\n";
    }
};

int main()
{
    setlocale(LC_ALL, "Russian");
    Animal* animals[]{ new Dog, new Cat, new Cow };
    for (unsigned i = 0; i < 3; i++)
    {
        animals[i]->Voice();
    }

}

