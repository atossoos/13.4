﻿#include <iostream>
#include <stdint.h>
#include <time.h>
using namespace std;


const int STR = 5;
const int COL = 5;


int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    buf.tm_mday;

    int line = buf.tm_mday % STR;

    const int size = 5;
    int array[STR][COL]{};
    int sum_of_line = 0;


    for (int i = 0; i < STR; i++)
    {
        for (int j = 0; j < COL; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j];
            if (i == line)
            {
                sum_of_line += array[i][j];
            }
            
        }
        cout << '\n';
    }
    cout << '\n';
    cout << sum_of_line << '\n';


}

