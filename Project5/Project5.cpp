﻿#include <iostream>
#include <cmath>

using namespace std;

class Vector
{
public:
    Vector(): x(1), y(2), z(3)
    {}
    void Length_Of_Vector()
    {
        cout << (sqrt(pow(x,2) + pow(y,2) + pow(z,2)));
    }
private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v;
    v.Length_Of_Vector();
}

