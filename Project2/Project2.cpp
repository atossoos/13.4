﻿

#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>
#include <conio.h>

using namespace std; //чтобы не писать каждый раз std

int main()
{
    cout << "Enter your password: " << "\n"; // приветствующая надпись для ввода пароля

    string password; // ввожу переменную пароль
    char ch; 
    const char ENTER = 13; // константа ENTER для подтверждения нажатием на ENTER
    while ((ch = _getch()) != ENTER) // Нашел на StackOverFlow как скрыть вводимый пароль звездочками
    {
        password += ch;
        std::cout << '*';
    }
    

    char first = password[0]; // первый символ в строке пароля поиск по индексу
    char last = password[password.size()-1]; // последний символ в строке пароля -1 от размера строки
    

    // вывод необходимых данных
    cout << "\n";
    cout << "Password's Length is " << password.length() << " chars" << "\n";
    cout << "\n";
    cout << "First char of password " << first << "\n";
    cout << "\n";
    cout << "Last char of password " << last << "\n";

    return 0;
}
