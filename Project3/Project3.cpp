﻿#include <iostream>
#include <stdint.h>



using namespace std; //чтобы не писать каждый раз std

void FindOddNumbers(int N, int i)
{
    
    cout << i << '\n';
}

int main()
{
    int N = 15;
    bool isOdd = false;

    for (int i = 0; i < N; ++i)
    {
        if (isOdd == true && i%2==0)
        {
            FindOddNumbers(N, i);
        }
        else if (isOdd == false && i % 2 != 0)
        {
            FindOddNumbers(N, i);
        }
    }
    
}

